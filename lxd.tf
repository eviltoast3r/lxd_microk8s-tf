locals {
  user_data = <<EOH

#cloud-config

package_update: true

package_upgrade: true

package_reboot_if_required: true

packages:
  - apt:
    - openvswitch-common
    - openvswitch-switch
    - openvswitch-switch-dpdk
    - nfs-common
    - curl
    - nano
    - iputils-ping
    - zip
    - unzip
    - jq
    - bridge-utils
    - nvme-cli
    - open-iscsi
  - snap:
    - [microk8s, --classic]

users:
  - default
  - name: ubuntu
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: microk8s
    ssh_authorized_keys:
      - ${trimspace(file("/config/.ssh/id_ed25519.pub"))}

EOH
  net_data  = <<EOH

network:
  version: 2
  ethernets:
    ${var.public_interface}:
      dhcp4: "true"
      critical: "true"
    ${var.cluster_interface}:
      dhcp4: "true"
      optional: "true"
      dhcp4-overrides:
        use-routes: "false"
        use-dns: "false"
        
EOH
}


resource "lxd_instance" "mk8s01" {
  name     = "mk8s01"
  image    = var.image_name
  type     = "virtual-machine"
  project  = var.lxd_project
  profiles = []
  device {
    name = "root"
    type = "disk"
    properties = {
      path            = "/"
      pool            = var.storage_pool
      size            = var.storage_size
      "boot.priority" = 10
    }
  }
  device {
    name = var.public_interface
    type = "nic"
    properties = {
      name    = var.public_interface
      nictype = "bridged"
      parent  = var.public_interface
    }
  }
  device {
    name = var.cluster_interface
    type = "nic"
    properties = {
      name    = var.cluster_interface
      network = var.cluster_interface
    }
  }
  limits = {
    cpu                = var.cpu_limit
    memory             = var.memory_limit
  }
  config = {
    "cloud-init.user-data"      = local.user_data
    "cloud-init.network-config" = local.net_data
    "user.access_interface"     = var.public_interface
    "agent.nic_config"          = "true"

  }
}

resource "time_sleep" "mk8s01_wait" {
  depends_on      = [lxd_instance.mk8s01]
  create_duration = "10s"
}

resource "lxd_instance" "mk8s02" {
  depends_on = [time_sleep.mk8s01_wait]
  name       = "mk8s02"
  image      = var.image_name
  type       = "virtual-machine"
  project    = var.lxd_project
  profiles   = []
  device {
    name = "root"
    type = "disk"
    properties = {
      path            = "/"
      pool            = var.storage_pool
      size            = var.storage_size
      "boot.priority" = 10
    }
  }
  device {
    name = var.public_interface
    type = "nic"
    properties = {
      name    = var.public_interface
      nictype = "bridged"
      parent  = var.public_interface
    }
  }
  device {
    name = var.cluster_interface
    type = "nic"
    properties = {
      name    = var.cluster_interface
      network = var.cluster_interface
    }
  }
  limits = {
    cpu                = var.cpu_limit
    memory             = var.memory_limit
  }
  config = {
    "agent.nic_config"          = "true"
    "cloud-init.user-data"      = local.user_data
    "cloud-init.network-config" = local.net_data
    "user.access_interface"     = var.public_interface
  }
}

resource "time_sleep" "mk8s02_wait" {
  depends_on      = [lxd_instance.mk8s02]
  create_duration = "10s"
}

resource "lxd_instance" "mk8s03" {
  depends_on = [time_sleep.mk8s02_wait]
  name       = "mk8s03"
  image      = var.image_name
  type       = "virtual-machine"
  project    = var.lxd_project
  profiles   = []
  device {
    name = "root"
    type = "disk"
    properties = {
      path            = "/"
      pool            = var.storage_pool
      size            = var.storage_size
      "boot.priority" = 10
    }
  }
  device {
    name = var.public_interface
    type = "nic"
    properties = {
      name    = var.public_interface
      nictype = "bridged"
      parent  = var.public_interface
    }
  }
  device {
    name = var.cluster_interface
    type = "nic"
    properties = {
      name    = var.cluster_interface
      network = var.cluster_interface
    }
  }
  limits = {
    cpu                = var.cpu_limit
    memory             = var.memory_limit
  }
  config = {
    "agent.nic_config"          = "true"
    "cloud-init.user-data"      = local.user_data
    "cloud-init.network-config" = local.net_data
    "user.access_interface"     = var.public_interface
  }
}

resource "time_sleep" "mk8s03_wait" {
  depends_on      = [lxd_instance.mk8s03]
  create_duration = "10s"
}

resource "lxd_instance" "mk8s04" {
  depends_on = [time_sleep.mk8s03_wait]
  name       = "mk8s04"
  image      = var.image_name
  type       = "virtual-machine"
  project    = var.lxd_project
  profiles   = []
  device {
    name = "root"
    type = "disk"
    properties = {
      path            = "/"
      pool            = var.storage_pool
      size            = var.storage_size
      "boot.priority" = 10
    }
  }
  device {
    name = var.public_interface
    type = "nic"
    properties = {
      name    = var.public_interface
      nictype = "bridged"
      parent  = var.public_interface
    }
  }
  device {
    name = var.cluster_interface
    type = "nic"
    properties = {
      name    = var.cluster_interface
      network = var.cluster_interface
    }
  }
  limits = {
    cpu                = var.cpu_limit
    memory             = var.memory_limit
  }
  config = {
    "agent.nic_config"          = "true"
    "cloud-init.user-data"      = local.user_data
    "cloud-init.network-config" = local.net_data
    "user.access_interface"     = var.public_interface
  }
}

resource "time_sleep" "network_interface_wait" {
  depends_on      = [lxd_instance.mk8s04]
  create_duration = "130s"
}