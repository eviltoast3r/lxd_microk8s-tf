## NETWORKING ####################################

variable "public_interface" {
  type    = string
  default = "apublic"
}

variable "cluster_interface" {
  type    = string
  default = "clstr"
}

variable "clstr_prefix" {
  type    = string
  default = "10.14"
}

## IMAGE CONFIG ####################################

variable "image_name" {
  type    = string
  default = "ubuntu-minimal:noble"
}

variable "huge_limit" {
  type    = string
  default = "4096"
}

variable "cpu_limit" {
  type    = string
  default = "4"
}

variable "memory_limit" {
  type    = string
  default = "16GiB"
}

## STORAGE ####################################

variable "storage_pool" {
  type    = string
  default = "default"
}

variable "storage_size" {
  type    = string
  default = "60GiB"
}

variable "block_source" {
  type    = string
  default = "/dev/sdb"
}

variable "block_path_01" {
  type    = string
  default = "/dev/sda"
}

variable "block_path_02" {
  type    = string
  default = "/dev/sdb"
}

variable "block_path_03" {
  type    = string
  default = "/dev/sdc"
}

variable "block_path_04" {
  type    = string
  default = "/dev/sdd"
}

## CREDENTIALS ####################################

variable "onepass_token" {
  type     = string
  default  = ""
  nullable = false
}

variable "onepass_vault_id" {
  type     = string
  default  = ""
  nullable = false
}

variable "onepass_lxd_id" {
  type     = string
  default  = ""
  nullable = false
}

variable "lxd_address" {
  type     = string
  default  = ""
}

variable "lxd_password" {
  type     = string
  default  = ""
}

variable "lxd_name" {
  type     = string
  default  = ""
  nullable = false
}

variable "lxd_project" {
  type    = string
  default = "default"
}

variable "clustr_prefix" {
  type    = string
  default = "10.14"
}

