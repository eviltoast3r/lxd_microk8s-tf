terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
    onepassword = {
      source  = "1Password/onepassword"
      version = "~> 1.3.0"
    }
    lxd = {
      source  = "terraform-lxd/lxd"
      version = ">= 2.0.0"
    }
  }
}

provider "onepassword" {
  service_account_token = var.onepass_token
}

data "onepassword_item" "lxd_login" {
  vault = var.onepass_vault_id
  uuid  = var.onepass_lxd_id
}

provider "lxd" {
  generate_client_certificates = true
  accept_remote_certificate    = true

  remote {
    name     = var.lxd_name
    scheme   = "https"
    address  = data.onepassword_item.lxd_login.url
    password = data.onepassword_item.lxd_login.password
    default  = true
  }
}