resource "terraform_data" "connect_cluster" {
  depends_on = [time_sleep.network_interface_wait]
  provisioner "local-exec" {
    when        = create
    interpreter = ["/bin/bash", "-c"]
    command     = <<-EOH
      echo "" > ~/.ssh/known_hosts
      token=$(ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s01.ipv4_address} "microk8s add-node | grep \"${var.clstr_prefix}\"")
      ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s02.ipv4_address} "$token --skip-verify"
      sleep 10s
      token=$(ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s02.ipv4_address} "microk8s add-node | grep \"${var.clstr_prefix}\"")
      ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s03.ipv4_address} "$token --skip-verify"
      sleep 10s
      token=$(ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s03.ipv4_address} "microk8s add-node | grep \"${var.clstr_prefix}\"")
      ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s04.ipv4_address} "$token --skip-verify"
      sleep 30s
      ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s01.ipv4_address} "microk8s config" > ./kubeconfig.yaml
      ssh -o StrictHostKeyChecking=no ubuntu@${lxd_instance.mk8s02.ipv4_address} "microk8s status --format yaml" > ./cluster.status
    EOH
  }
}

resource "local_file" "lxd_names" {
  depends_on = [terraform_data.connect_cluster]
  filename   = "./ip.json"
  content    = <<-EOH
    {
      "${lxd_instance.mk8s01.name}" : {
        "ip" : "${lxd_instance.mk8s01.ipv4_address}"
      },
      "${lxd_instance.mk8s02.name}" : {
        "ip" : "${lxd_instance.mk8s02.ipv4_address}"
      },
      "${lxd_instance.mk8s03.name}" : {
        "ip" : "${lxd_instance.mk8s03.ipv4_address}"
      },
      "${lxd_instance.mk8s04.name}" : {
        "ip" : "${lxd_instance.mk8s04.ipv4_address}"
      }
    }
  EOH
}