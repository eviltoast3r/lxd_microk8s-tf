# Microk8s Cluster on LXD using Terraform

This repo will create a 4 node (3 active, 1 standby) Microk8s cluster on top of an existing LXD host or cluster.

Most config options can be overridden via `terraform.tfvars` see `variables.tf` for options

## Requirements

### Networking
- 1 bridge attached to an internet connection with DHCP called `apublic`
    - This needs to come up first or cloud-init will not run and install microk8s
    - Interfaces are brought up in alphebetical order, adding the `a` ensures it comes up first
- 1 bridge with with DHCP and no uplinks for cluster commuication called `clstr`
    - It is necessary to define the cluster subnet prefix so terraform knows which network to set for cluster communication
        - Defaults to `10.14`, set by `var.clstr_prefix`

### Disk
- Root disk
    - Points to default storage pool
    - Defaults to `60GiB`

## Configuration

- 1password Secrets Storage
    - Requires a 1password subscription
        - See LXD for manual config
    - Populate `onepass_token` with your 1password [Service Account Token](https://developer.1password.com/docs/service-accounts/get-started/)
    - Populate `onepass_lxd_id` with the 1P item UUID and `onepass_vault_id` with the 1P UUID of the vault containing it
        - If you copy a private link to any item in the vault `v=blah` will be part of the link, `blah` is your vault UUID. Using this same method you can also find the Item UUID of any item by looking at `i=blah`
- LXD
    - Default values stored in 1P
        - Must be a login type object with LXD's `core.trust_password` set as the password and IP/DNS Name as the URL.
        - Username does not matter.
    - To use hard-coded creds change the values of `lxd_address` and `lxd_password` in `terraform.tfvars` to match your environment
    - OS defaults to Ubuntu minimal Noble LTS build (24.04)
        - This is the only one I can get to work with cloud-init, no idea why
        - You can change it via `var.image_name`
- SSH Keys
    - The machine you execute the TF from will need non-interactive SSH access to the VMs
    - This is done by pulling a user's local SSH keys from `/config/.ssh/id_ed25519.pub` 

> Yes, this *should* have been done in containers (VMs are bloated/overkill, yes, yes, I know), however, I find NFS to be essential to a K8s cluster and it is currently not possible (without some real hacky stuff) to mount an NFS share directly inside a container.  
> Stéphane Graber (Maintainer of [Incus](https://linuxcontainers.org/incus/)) gives the best explanation of this issue that I have found to date: https://discuss.linuxcontainers.org/t/nfs-directly-inside-container/9841  
> Side note, this dude is amazing, he comments on nearly every linuxcontainers forum post and is always extremely helpful, I completely credit my understanding of LXC/LXD to his comments/documentation.